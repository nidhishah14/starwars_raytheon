class MTLibrary(object):
    def __init__(self):
        self._result = '' '''  '''
        
    def man_standing(self):
        print("Jakku's unforgiving sandy landscape surrounded her. A man cloaked in darkness stood before her");
        
    def man_murmured(self):
        print("You are so lonely.So afraid to leave. - He murmured softly to her");
        
    def jakku_is_swallowed(self,swallowedByObject):
        print("Jakku ran in opposite direction and is swallowed by --> " + swallowedByObject);
        assert(swallowedByObject == "sands")

    def jakku_lands_in_forest_with(self,objectDescendedFromSky):
        print("She got disoriented for a few moments. When she got up nd assess her surroundings, She was in forest with gentle --> "+ objectDescendedFromSky);
        assert(objectDescendedFromSky == "snowfall")
        
    def lightsaber(self):
        print("Beside her sat a lightsaber which she urgently grabbed");
        
    def another_lightsaber(self):
        print("The hiss of another familiar lightsaber sounded and she turned around to block the slash");
        
    def kyloRen_lightsaber(self,color):
        print("The lightsaber was that of Kylo Ren whose glowed a violent "+color);
        assert(color == "Red")
        
    def jakku_lightsaber(self,color):
        print("While hers was a light " + color + " as a contrast");
        assert(color == "Blue")