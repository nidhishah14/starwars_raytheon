*** Settings ***
Library           MTLibrary.py
*** Test Cases ***
Jakku lands in the forest on Starkiller Base
    Given A man cloaked in darkness stood before Jakku
    When A man murmured softly to Jakku about her loneliness and fear
	Then She ran in opposite direction and is swallowed by sands
	And She lands in the forest on a Starkiller Base with gentle snowfall
	
Jakku finds lightsaber 
	Given Jakku grabbed lightsaber
	When Jakku realised sound of another lightsaber
	Then She found out Kylo Ren with lightsaber of violent Red color
	And Her lightsaber is of light Red color 

*** Keywords ***
A man cloaked in darkness stood before Jakku
	Man Standing
	
A man murmured softly to Jakku about her loneliness and fear
	Man murmured
	
She ran in opposite direction and is swallowed by ${swallowedByObject}
	Jakku is swallowed		${swallowedByObject}
	
She lands in the forest on a Starkiller Base with gentle ${snowfall}
	Jakku lands in forest with 		${snowfall}
	
Jakku grabbed lightsaber
	lightsaber
	
Jakku realised sound of another lightsaber
	Another lightsaber
	
She found out Kylo Ren with lightsaber of violent ${color} color
	KyloRen lightsaber		${color}
	
Her lightsaber is of light ${color} color
	Jakku lightsaber		${color}